
%%Create Objects
%%Creating our on-pixels
%set(gcf,'PaperUnits','inches','PaperPosition',[0 0 0.49 0.49], 'Color', 'w');
%xlim = 49;
%ylim = 49;
%x=rand(1,48);
%y=rand(1,48);
%scatter(x, y, 'k');
%total N of elements
%Number_total = 64;
%N_ones = 10;
%v = zeros(Number_total,1);
%vector with desired entries
%v(1:N_ones,1) = 1;
%reshape
%A = reshape(v(randperm(Number_total)),8,8);

%axis equal;
%axis off;
%%Save Image
%N of images
Imn = 400;
for ii = 1:Imn
    Number_total = 2368170;
    N_ones = randi(10000);
    v = zeros(Number_total,1);
    %vector with desired entries
    v(1:N_ones,1) = 1;
    %reshape
    %can do number total = n*m, but need to agree with parameters down
    A = reshape(v(randperm(Number_total)),1253,1890);
    imwrite(A,strcat('C:\Users\dell\Desktop\Quantum\dmd-and-fresnel-propagation-master\training\control_patterns\pattern', num2str(ii),'.bmp'));
end
%img = getframe(gcf);
%img = rand(x);