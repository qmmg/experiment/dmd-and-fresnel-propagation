% simple test code for the square Zernike polynomials
% the idea is that this will eventually turn into a set of DMD images that
% can be used to generate a set of "basis images" for the imaging system
% I hope to try with Chebyshev and any other sets I can think of

% Zernike polynomials can also be used to model aberrations in the system

close all; clc

%% DMD parameters

spx = 7; %superpixel size

%regular pixels
px_x = 1920;
px_y = 1080;

%superpixels
spx_x = floor(px_x/spx); %we take the floor because we need integers
spx_y = floor(px_y/spx); %half a superpixel will do us little good, so we take the floor

DMD = zeros(px_x, px_y);
sDMD = zeros(spx_x, spx_y);

x = linspace(-1, 1, px_x);
y = linspace(-1, 1, px_y);

xs = linspace(-1, 1, spx_x);
ys = linspace(-1, 1, spx_y);

[X,Y] = meshgrid(x,y);
RHO_SQ = X.^2 + Y.^2;

[XS, YS] = meshgrid(xs,ys);
RHOS_SQ = XS.^2 + YS.^2;

%% Make me some polynomials, Jeeves

% These are simply taken from the paper found here:
% https://doi.org/10.1364/JOSAA.31.002304
% I am currently hard-coding these in in a very lazy way, but plan to
% modify the code to use the generating definitions for the circular guys
% + the transformation matrix

% use first 10 pnoms
% not normalized to 1

zp1 = 1/2*ones(size(X));
zp2 = sqrt(3)*X/2;
zp3 = sqrt(3)*Y/2;
zp4 = (1/2)*sqrt(5/2)*(3*RHO_SQ - 2)/2;
zp5 = 3*X.*Y/2;
zp6 = (3/2)*sqrt(5/2)*(X.^2 - Y.^2)/2;
zp7 = (1/2)*sqrt(21/62)*(15*RHO_SQ - 14).*Y/2;
zp8 = (1/2)*sqrt(21/62)*(15*RHO_SQ - 14).*X/2;
zp9 = (1/2)*sqrt(5/62)*(27*X.^2 - 35*Y.^2 + 12).*Y/2;
zp10 = (1/2)*sqrt(5/62)*(35*X.^2 - 27*Y.^2 + 12).*X/2;

zp1s = 1/2*ones(size(XS));
zp2s = sqrt(3)*XS/2;
zp3s = sqrt(3)*YS/2;
zp4s = (1/2)*sqrt(5/2)*(3*RHOS_SQ - 2)/2;
zp5s = 3*XS.*YS/2;
zp6s = (3/2)*sqrt(5/2)*(XS.^2 - YS.^2)/2;
zp7s = (1/2)*sqrt(21/62)*(15*RHOS_SQ - 14).*YS/2;
zp8s = (1/2)*sqrt(21/62)*(15*RHOS_SQ - 14).*XS/2;
zp9s = (1/2)*sqrt(5/62)*(27*XS.^2 - 35*YS.^2 + 12).*YS/2;
zp10s = (1/2)*sqrt(5/62)*(35*XS.^2 - 27*YS.^2 + 12).*XS/2;

zp = {zp1, zp2, zp3, zp4, zp5, zp6, zp7, zp8, zp9, zp10};
zps = {zp1s, zp2s, zp3s, zp4s, zp5s, zp6s, zp7s, zp8s, zp9s, zp10s};

%map between 0 and 1
for i = 2:10
    maxzp = max(max(zp{i}));
    minzp = min(min(zp{i}));
    zp{i} = (zp{i} - minzp)/(maxzp - minzp);
    maxzps = max(max(zps{i}));
    minzps = min(min(zps{i}));
    zps{i} = (zps{i} - minzps)/(maxzps - minzps);
end

figure
for i = 2:10
    subplot(3,3,i-1)
    imagesc(x, y, zp{i})
    colorbar
    title(['Zernike ', num2str(i)])
end

figure
for i = 2:10
    subplot(3,3,i-1)
    imagesc(xs, ys, zps{i})
    colorbar
    title(['Zernike S ', num2str(i)])
end

zp_disc = cell(size(zps));

%% now map this onto DMD images using the Floyd_Steinberg function

% % This code discretizes the function on the superpixel array
% % maps values from 0-49 (number of pixels on)
% nvals = 50;
% mappx = linspace(0,1,nvals);
% disc = 1/49;
% for i = 1:10
%     zp_disc{i} = round(zps{i}/disc);
% end
% 
% figure
% for i = 2:10
%     subplot(3,3,i-1)
%     imagesc(x, y, zp_disc{i})
%     colorbar
%     title(['Zernike disc ', num2str(i)])
% end

DMD_imgs = cell(size(zp));
for i = 2:10
    DMD_imgs{i} = Floyd_Steinberg(1,zp{i});
end

figure('Position', [100 100 1200 800])
for i = 2:10
    subplot(3,3,i-1)
    imagesc(x, y, DMD_imgs{i})
    colorbar
    colormap gray
    title(['DMD img ', num2str(i)])
    pbaspect([1 1 1])
end