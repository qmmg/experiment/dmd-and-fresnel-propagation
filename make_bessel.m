function Eout = make_bessel(NA, a, zp, f, lambda, x0, y0)

global X2D Y2D

%code to make a Bessel beam
%this is the point-spread function (PSF) of our imaging system
%contact C. Weidner for references on what this code is doing

k = 2*pi/lambda;

N = pi*a^2/(lambda*zp);
rho = (2*pi*NA/lambda)*sqrt((X2D-x0).^2 + (Y2D-y0).^2);

Eout = 1i*exp(-1i*k*f).*exp(1i*rho.^2/(4*N)).*(2*besselj(1, rho)./rho);
%the following code just takes care of numerical divide-by-zero errors that
%are not actually a problem in real life
if ~isempty(Eout((X2D-x0) == 0 & (Y2D-y0) == 0)) && isnan(Eout((X2D-x0) == 0 & (Y2D-y0) == 0))
    Eout((X2D-x0) == 0 & (Y2D-y0) == 0) = 1i*exp(-1i*k*f);
end