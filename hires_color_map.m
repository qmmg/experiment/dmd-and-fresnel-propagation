function mycolormap = hires_color_map(steps) % should be divisible by three
 mycolorbasis = [0 0 0 ; 68 27 143; 255 85 249; 255 255 255]/255; % pink color
 %mycolorbasis = [36 15 77 ; 68 27 143 ; 255 85 249; 255 255 255]/255; % pink color

 %mycolorbasis = [68 27 143; 255 85 249; 255 255 255]/255; % pink color

 %mycolorbasis = [255 255 255; 255 85 249; 68 27 143; 0 0 0]/255; % pink color

 %mycolorbasis = [0 0 0 ; 68 27 143; 119 255 85; 255 255 255]/255; % green color

steps = linspace(0,1,steps);
color_steps = linspace(0,1,4);
%color_steps = linspace(0,1,3);

mycolormap = interp1(color_steps,mycolorbasis,steps);

% colormap(mycolormap1);
% surf(peaks)
% colorbar

