%% Generates the DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_2 = px_x/2;
px_y_2 = px_y/2;

%% Save folder
folderstr = 'DMD_Hilbert_imgs';
savefile = ['./', folderstr, '/'];

%% First, do Hilbert stuff

% % SIZE 9
% 
% % The highest order curve we can fit on our DMD is the 9th order Hilbert
% % curve. How big is it? (hint: 1025 px)
% 
% max_order = 9;
% 
% %create Hilbert images
% n_H_imgs = 2*max_order;
% 
% % image positions
% H_size = max(size(Hilbert_curve(max_order,0)));
% H_half = ceil(H_size/2);
% 
% pos_x = 1 - H_half;
% pos_y = 1 - H_half;
% 
% for i = 3
%     pos_x(i-2) = pos_x(i-2) + (i-1)*px_y/4 + ceil((px_x-px_y)/2);
%     pos_y(i-2) = pos_y(i-2) + (i-1)*px_y/4;
% end
% 
idx = 0;
% 
% for i = 1:n_H_imgs
%     order = ceil(i/2);
%     inv = 1 - mod(i,2);
%     H_imgs{1} = Hilbert_curve(order, inv);
%     file_preamble = ['Hilbert', num2str(max_order), 'order', num2str(ceil(i/2)), ...
%         'inv', num2str(1 - mod(i,2)), '_1000000_False_1'];
%     make_DMD_image(H_imgs, {[pos_x, pos_y]}, H_size, file_preamble, idx, savefile);
%     idx = idx + 1;
% end
% 
% % SIZE 8
% 
% % Now try the 8th order curve
% 
% max_order = 8;
% 
% %create Hilbert images
% n_H_imgs = 2*max_order;
% 
% % image positions
% H_size = max(size(Hilbert_curve(max_order,0)));
% H_half = ceil(H_size/2);
% 
% pos_x = 1 - H_half;
% pos_y = 1 - H_half;
% 
% for i = 3
%     pos_x(i-2) = pos_x(i-2) + (i-1)*px_y/4 + ceil((px_x-px_y)/2);
%     pos_y(i-2) = pos_y(i-2) + (i-1)*px_y/4;
% end
% 
% for i = 1:n_H_imgs
%     order = ceil(i/2);
%     inv = 1 - mod(i,2);
%     H_imgs{1} = Hilbert_curve(order, inv);
%     file_preamble = ['Hilbert_', num2str(max_order), '_order_', num2str(ceil(i/2)), ...
%         '_inv_', num2str(1 - mod(i,2)), '_1000000_False_1'];
%     make_DMD_image(H_imgs, {[pos_x, pos_y]}, H_size, file_preamble, idx, savefile);
%     idx = idx + 1;
% end

% SIZE 7

% Now try the 7th order curve

max_order = 7;

%create Hilbert images
n_H_imgs = 2*max_order;

% image positions
H_size = max(size(Hilbert_curve(max_order,0)));
H_half = ceil(H_size/2);

pos_x = 1 - H_half;
pos_y = 1 - H_half;

for i = 3
    pos_x(i-2) = px_x_2 + pos_x(i-2) - 100;
    pos_y(i-2) = px_y_2 + pos_y(i-2) - 200;
end

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    H_imgs{1} = Hilbert_curve(order, inv);
    file_preamble = ['Hilbert', num2str(max_order), 'order', num2str(ceil(i/2)), ...
        'inv', num2str(1 - mod(i,2)), '_1000000_False_1'];
    make_DMD_image(H_imgs, {[pos_x, pos_y]}, H_size, file_preamble, idx, savefile);
    idx = idx + 1;
end