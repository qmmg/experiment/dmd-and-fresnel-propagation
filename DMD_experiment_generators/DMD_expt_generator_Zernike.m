%% Generates the Zernike DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%% Save folder
folderstr = 'DMD_Zernike_imgs';
savefile = ['./', folderstr, '/'];

%% Now Zernike

% SIZE 1080

min_order_Z = 2;
max_order_Z = 10;

%create Hilbert images
n_Z_imgs = 2*(max_order_Z-min_order_Z);

Z_size = px_y;
Z_half = ceil(Z_size/2);

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

% index for images
idx = 0;
Z_imgs = {1};
for i = 1:n_Z_imgs
    order = ceil(i/2) + 1;
    inv = 1 - mod(i,2);
    Z_imgs{1} = square_Zernike(order, inv, Z_size);
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1),...
                '_1000000_False_1'];
            make_DMD_image(Z_imgs, {[pos_x(j), pos_y(k)]}, Z_size, file_preamble, idx, savefile);
            idx = idx + 1;
        end
    end
end

% SIZE 540

Z_size = px_y/2;
Z_half = ceil(Z_size/2);

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_Z_imgs
    order = ceil(i/2) + 1;
    inv = 1 - mod(i,2);
    Z_imgs{1} = square_Zernike(order, inv, Z_size);
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1),...
                '_1000000_False_1'];
            make_DMD_image(Z_imgs, {[pos_x(j), pos_y(k)]}, Z_size, file_preamble, idx, savefile);
            idx = idx + 1;
        end
    end
end

% SIZE 360

Z_size = px_y/3;
Z_half = ceil(Z_size/2);

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_Z_imgs
    order = ceil(i/2) + 1;
    inv = 1 - mod(i,2);
    Z_imgs{1} = square_Zernike(order, inv, Z_size);
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1),...
                '_1000000_False_1'];
            make_DMD_image(Z_imgs, {[pos_x(j), pos_y(k)]}, Z_size, file_preamble, idx, savefile);
            idx = idx + 1;
        end
    end
end