%% Generates the DMD images

%% General DMD stuff

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%half-tiling
px_x_2 = px_x/2;
px_y_2 = px_y/2;

%range
rng = 245;

%% Save folder
folderstr = 'DMD_circles';
savefile = ['./', folderstr, '/'];

%% Circles!

% R7

% Image size is 1080 px

circle_number = 1;
circle_rad = 7;
circle_dist = 70;
rot = 0;
mkline = 0;

% image positions
circ_size = px_y;
circ_half = ceil(circ_size/2);

pos_x = ones(1,15) - circ_half;
pos_y = ones(1,15) - circ_half;

len = 11;
for i = 1:len
    pos_x(i) = px_x_2  + (i-(len-1)/2 + 1)*ceil(rng/(len-1)) - circ_half - 100;
    pos_y(i) = px_y_2 + (i-(len-1)/2 + 1)*ceil(rng/(len-1)) - circ_half - 200;
end

% index for images
idx = 0;

imgs = {1};

imgs{1} = make_circles(circle_number, circle_rad, circle_dist, rot, mkline);
for j = 1:len
    for k = 1:len
        file_preamble = ['R', num2str(circle_rad), ...
            'x', num2str(j-1), 'y', num2str(k-1), '_1000000_False_1'];
        make_DMD_image(imgs, {[pos_x(j), pos_y(k)]}, circ_size, file_preamble, idx, savefile);
        idx = idx + 1;
    end
end