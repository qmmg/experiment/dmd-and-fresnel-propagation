function img = Hilbert_curve(order, invert)

% Generates a Hilbert curve of the given order
% toggling "invert" to 1 gives the compliment of the original image

% algorithm stolen shamelessly from
% https://blogs.mathworks.com/steve/2012/01/25/generating-hilbert-curves/

px = 2^(order + 1) + 1; %assumes square

x = zeros(1,px);
y = x;
img = zeros(px);

z = 0;

a = 1 + 1i;
b = 1 - 1i;

for k = 1:order
    w = 1i*conj(z);
    z = [w-a; z-b; z+a; b-w]/2;
end

zx = (2^order)*real(z) + 2^(order) + 1;
zy = (2^order)*imag(z) + 2^(order) + 1;

len = length(zx);

for i = 1:len-1
    if zx(i) <= zx(i+1) && zy(i) <= zy(i+1)
        img(zx(i):zx(i+1), zy(i):zy(i+1)) = 1;
    elseif zx(i) <= zx(i+1) && zy(i) >= zy(i+1)
        img(zx(i):zx(i+1), zy(i):-1:zy(i+1)) = 1;
    elseif zx(i) >= zx(i+1) && zy(i) <= zy(i+1)
        img(zx(i):-1:zx(i+1), zy(i):zy(i+1)) = 1;
    else
        img(zx(i):-1:zx(i+1), zy(i):-1:zy(i+1)) = 1;
    end
end

if invert == 1
    img = -img + 1;
end

