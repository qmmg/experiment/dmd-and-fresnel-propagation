function img = square_Zernike(idx, inv, varargin)

% simple function to generate the square Zernike polynomials
% outputs polynomial with index idx (up to the 10th) with optional size
% denoted by varargin

% Zernike polynomials can also be used to model aberrations in the system

close all; clc

%% DMD parameters

if nargin == 3
    px = varargin{1};
elseif nargin > 3
    error('Too many input arguments.')
else
    px = 1080;
end

if idx < 2 || idx > 10 || mod(idx,1) ~= 0 || length(idx) > 1
    error('Index value should be a single positive integer greater than 2 and less than 11.')
end

DMD = zeros(px, px);

x = linspace(-1, 1, px);
y = linspace(-1, 1, px);

[X,Y] = meshgrid(x,y);
RHO_SQ = X.^2 + Y.^2;

%% Make me some polynomials, Jeeves

% These are simply taken from the paper found here:
% https://doi.org/10.1364/JOSAA.31.002304
% I am currently hard-coding these in in a very lazy way, but plan to
% modify the code to use the generating definitions for the circular guys
% + the transformation matrix

% use first 10 pnoms
% not normalized to 1

zp1 = 1/2*ones(size(X)); % this one is not used
zp2 = sqrt(3)*X/2;
zp3 = sqrt(3)*Y/2;
zp4 = (1/2)*sqrt(5/2)*(3*RHO_SQ - 2)/2;
zp5 = 3*X.*Y/2;
zp6 = (3/2)*sqrt(5/2)*(X.^2 - Y.^2)/2;
zp7 = (1/2)*sqrt(21/62)*(15*RHO_SQ - 14).*Y/2;
zp8 = (1/2)*sqrt(21/62)*(15*RHO_SQ - 14).*X/2;
zp9 = (1/2)*sqrt(5/62)*(27*X.^2 - 35*Y.^2 + 12).*Y/2;
zp10 = (1/2)*sqrt(5/62)*(35*X.^2 - 27*Y.^2 + 12).*X/2;

zp = {zp1, zp2, zp3, zp4, zp5, zp6, zp7, zp8, zp9, zp10};

img = zp{idx};

% invert (if flagged) map between zero and one
if inv == 1
    img = -img;
end
maxzp = max(max(img));
minzp = min(min(img));
img = (img - minzp)/(maxzp - minzp);