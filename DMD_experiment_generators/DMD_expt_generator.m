%% Generates the DMD images

close all; clc

%% General DMD stuff
tic

px_x = 1920;
px_y = 1080;

%quarter-tiling
px_x_4 = px_x/4;
px_y_4 = px_y/4;

%% First, do Hilbert stuff

% SIZE 9

% The highest order curve we can fit on our DMD is the 9th order Hilbert
% curve. How big is it? (hint: 1025 px)

max_order = 9;

%create Hilbert images
n_H_imgs = 2*max_order;

H_imgs = cell(1, n_H_imgs);

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    H_imgs{i} = Hilbert_curve(order, inv);
end

% image positions
H_size = max(size(Hilbert_curve(max_order,0)));
H_half = ceil(H_size/2);

pos_x = ones(1,5) - H_half;
pos_y = ones(1,5) - H_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

% index for images
idx = 0;

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Hilbert_', num2str(max_order), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1)];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

% SIZE 8

% Now try the 8th order curve

max_order = 8;

%create Hilbert images
n_H_imgs = 2*max_order;

H_imgs = cell(1, n_H_imgs);

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    H_imgs{i} = Hilbert_curve(order, inv);
end

% image positions
H_size = max(size(Hilbert_curve(max_order,0)));
H_half = ceil(H_size/2);

pos_x = ones(1,5) - H_half;
pos_y = ones(1,5) - H_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Hilbert_', num2str(max_order), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1)];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

% SIZE 7

% Now try the 8th order curve

max_order = 7;

%create Hilbert images
n_H_imgs = 2*max_order;

H_imgs = cell(1, n_H_imgs);

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    H_imgs{i} = Hilbert_curve(order, inv);
end

% image positions
H_size = max(size(Hilbert_curve(max_order,0)));
H_half = ceil(H_size/2);

pos_x = ones(1,5) - H_half;
pos_y = ones(1,5) - H_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Hilbert_', num2str(max_order), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1), ...
                '_1000000_False_1'];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

%% Now Zernike

% SIZE 1080

min_order_Z = 2;
max_order_Z = 10;

%create Hilbert images
n_Z_imgs = 2*(max_order_Z-min_order_Z);

Z_imgs = cell(1, n_Z_imgs);

Z_size = px_y;
Z_half = ceil(Z_size/2);

for i = min_order_Z:max_order_Z
    order = ceil(i/2) + 1;
    inv = 1 - mod(i,2);
    Z_imgs{i} = square_Zernike(order, inv, Z_size);
end

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

% index for images
idx = 0;

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1)];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

% SIZE 540

Z_size = px_y/2;
Z_half = ceil(Z_size/2);

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    Z_imgs{i} = square_Zernike(order, inv, Z_size);
end

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1)];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

% SIZE 360

Z_size = px_y/3;
Z_half = ceil(Z_size/2);

for i = 1:n_H_imgs
    order = ceil(i/2);
    inv = 1 - mod(i,2);
    Z_imgs{i} = square_Zernike(order, inv, Z_size);
end

% image positions

pos_x = ones(1,5) - Z_half;
pos_y = ones(1,5) - Z_half;

for i = 2:5
    pos_x(i) = pos_x(i) + (i-1)*px_x/4;
    pos_y(i) = pos_y(i) + (i-1)*px_y/4;
end

for i = 1:n_H_imgs
    for j = 1:5
        for k = 1:5
            file_preamble = ['Zernike_', num2str(Z_size), '_order_', num2str(ceil(i/2)), ...
                '_inv_', num2str(1 - mod(i,2)), '_x_', num2str(j-1), '_y_', num2str(k-1)];
            make_DMD_image({H_imgs{i}}, {[pos_x(j), pos_y(k)]}, H_size, file_preamble, idx);
            idx = idx + 1;
        end
    end
end

%% Now do arrays and other things

toc