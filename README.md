# DMD and Fresnel propagation

Given Rb-87 atoms trapped in a three-dimensional optical lattice and imaged with a 0.7 NA microscope objective. We can also project potentials onto the atoms using DMDs and propagate that potential through space using Fresnel diffraction.